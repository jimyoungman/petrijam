﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Foods Foods;
    public Jobs Jobs;
    public PlayerData PlayerData;
    public JobData ChosenJob = null;

    public GameUIManager _gameUIManager;

    private GameState _gameState = GameState.Setup;

    public AudioManager AudioManager;

    private int day = 0;

    private bool Started = false;

    public GameObject TitleScreen;
    public Text MealText;


    // Start is called before the first frame update
    void Start()
    {
        _gameUIManager.HideWin();
        AudioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();

        PlayerData.Setup();
        //LoadPlayerData();

        _gameUIManager.SetHealthData(PlayerData);

        LoadFoodData();
        LoadJobData();

        _gameState = GameState.Setup;
        ProgressTime();

        _gameUIManager.DayText.text = "Day: " + (day + 1);

    }


    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    PlayerData.Setup();
        //    _gameUIManager.SetHealthData(PlayerData);

        //    _gameState = GameState.Setup;
        //    ProgressTime();
        //}
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!Started)
            {
                Started = true;
                TitleScreen.SetActive(false);
                PlayerData.Setup();
                _gameUIManager.SetHealthData(PlayerData);

                _gameState = GameState.Setup;
                ProgressTime();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void OnSelectJob(JobData jobData)
    {
        
        if (jobData != null)
        {
            ChosenJob = jobData;
            _gameUIManager.SetJobData(ChosenJob);
        }
        else
        {
            _gameState = GameState.Setup;
        }
        ProgressTime();
    }


    // Update is called once per frame
    private void SetupCurrentState()
    {
        _gameUIManager.SetupCurrentState(_gameState, Foods, Jobs);
    }

    public void PlayAgian()
    {
        _gameUIManager.HideWin();
        PlayerData.Setup();
        _gameUIManager.SetHealthData(PlayerData);

        _gameState = GameState.Setup;
        day = 0;
        _gameUIManager.DayText.text = "Day: " + (day + 1);

        ProgressTime();
    }

    public void OnSelectMeal(FoodData foodData)
    {
        if (foodData != null)
        {
            PlayerData.AddFood(foodData);
        }
        ProgressTime();
    }

    private void ProgressTime()
    {
        switch (_gameState)
        {
            case GameState.Setup:
                _gameState = GameState.JobSelect;
                MealText.text = "Select Job";
                break;
            case GameState.JobSelect:
                _gameState = GameState.SelectBreakfast;
                MealText.text = "Select Breakfast";
                break;
            case GameState.SelectBreakfast:
                _gameState = GameState.SelectSnack;
                MealText.text = "Select Snack";
                break;
            case GameState.SelectSnack:
                _gameState = GameState.SelectLunch;
                MealText.text = "Select Lunch";
                break;
            case GameState.SelectLunch:
                _gameState = GameState.SelectActivity;
                MealText.text = "Select Activity";
                break;
            case GameState.SelectActivity:
                _gameState = GameState.SelectDinner;
                MealText.text = "Select Dinner";
                break;
            case GameState.SelectDinner:
                PlayerData.EndDay();
                if (day < 6)
                {
                    _gameUIManager.UpdateHealthViews(PlayerData);
                    day++;
                    _gameUIManager.DayText.text = "Day: " + (day + 1);  
                    _gameState = GameState.SelectBreakfast;
                    MealText.text = "Select Breakfast";

                }
                else
                {
                    _gameState = GameState.JobDecision;
                    MealText.text = "";

                    _gameUIManager.ShowWin(DidPlayerWin());
                }
                break;
            case GameState.JobDecision:
                _gameState = GameState.JobSelect;
                
                break;
        }

        SetupCurrentState();
    }

    private bool DidPlayerWin()
    {
        return PlayerData.CheckWin(ChosenJob);
    }

    private void LoadPlayerData()
    {
        if (!System.IO.File.Exists(Application.persistentDataPath + "PlayerSaveData.json"))
        {
            return;
        }

        string playerDataFile = System.IO.File.ReadAllText(Application.persistentDataPath + "PlayerSaveData.json");
        PlayerData = JsonUtility.FromJson<PlayerData>(playerDataFile);
        Debug.Log($"Player Data Loaded");

    }

    public void SavePlayerData()
    {

        string data = JsonUtility.ToJson(PlayerData);
        System.IO.File.WriteAllText(Application.persistentDataPath + "PlayerSaveData.json", data);
        Debug.Log($"Player Data Saved");
    }

    public void LoadFoodData()
    {
        TextAsset foodDataFile = Resources.Load<TextAsset>("FoodData");
        Foods = JsonUtility.FromJson<Foods>(foodDataFile.text);
        Debug.Log($"Food Data Loaded");
    }

    public void LoadJobData()
    {
        TextAsset jobDataFile = Resources.Load<TextAsset>("JobData");
        Jobs = JsonUtility.FromJson<Jobs>(jobDataFile.text);
        Debug.Log($"Job Data Loaded");
    }



    private void OnApplicationFocus(bool focus)
    {
        if(!focus)
        {
            //SavePlayerData();
        }
    }

    private void OnApplicationQuit()
    {
        //SavePlayerData();
    }
}