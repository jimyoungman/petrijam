﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinView : MonoBehaviour
{
    public Text WinText;


    public void ShowWin(bool win)
    {
        gameObject.SetActive(true);
        if (win)
        {
            WinText.text = "You got the Job!";
        }
        else
        {
            WinText.text = "You didn't got the Job!";
        }
    }

    public void HideWin()
    {
        gameObject.SetActive(false);
    }
}
