﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundData
{
    public AudioClip AudioClip;
    [HideInInspector]
    public AudioSource Source;

    public bool Loop;

    [Range(0f,1f)]
    public float Volume;

    public string ID;
}
