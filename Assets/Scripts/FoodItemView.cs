﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodItemView : MonoBehaviour
{
    public FoodData FoodData;
    public JobData JobData;
    public event Action<FoodData> SelectFoodItem;
    public event Action<JobData> SelectJobItem;


    //UI
    public Text FoodTitle;
    public Image Sprite;

    public void SetFood(FoodData foodData)
    {
        JobData = null;
        FoodData = foodData;

        FoodTitle.text = foodData.ID;

        Sprite sprite = Resources.Load<Sprite>($"Sprites/{foodData.SpriteID}");
            Sprite.sprite = sprite;
    }

    public void SetJob(JobData jobData)
    {
        FoodData = null;
        JobData = jobData;

        FoodTitle.text = jobData.ID;

        Sprite sprite = Resources.Load<Sprite>($"Sprites/{jobData.SpriteID}");
            Sprite.sprite = sprite;
    }

    public void SelectDataItem()
    {
        if (FoodData != null)
        {
            SelectFoodItem?.Invoke(FoodData);
        }
        else if (JobData != null)
        {
            SelectJobItem?.Invoke(JobData);
        }
    }
}
