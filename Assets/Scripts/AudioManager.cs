﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public SoundData[] Sounds;

    private void Start()
    {
        AddAudioSources();
    }

    public void AddAudioSources()
    {
        foreach (SoundData soundData in Sounds)
        {
            soundData.Source = gameObject.AddComponent<AudioSource>();
            soundData.Source.clip = soundData.AudioClip;
            soundData.Source.volume = soundData.Volume;
            soundData.Source.loop = soundData.Loop;
        }
    }

    public void Play(string ID)
    {
        SoundData soundData = Array.Find(Sounds, SoundData => SoundData.ID == ID);
        if(soundData == null)
        {
            return;
        }
        soundData.Source.Play();
    }
}
