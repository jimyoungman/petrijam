﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Setup,
    JobSelect,
    SelectBreakfast,
    SelectSnack,
    SelectLunch,
    SelectActivity,
    SelectDinner,
    JobDecision
}
