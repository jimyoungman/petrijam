﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JobData
{
    public string ID;
    public float Weight;
    public float Heart;
    public float Acne;
    public float Dental;
    public float Muscle;
    public string SpriteID;
}
