﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JobView : MonoBehaviour
{
    public Text MuscleText;
    public Text HeartText;
    public Text AcneText;
    public Text DentalText;


    public void SetJobTargets(JobData jobData)
    {
        MuscleText.text = jobData.Muscle.ToString("F2");
        HeartText.text = jobData.Heart.ToString("F2");
        AcneText.text = jobData.Acne.ToString("F2");
        DentalText.text = jobData.Dental.ToString("F2");
    }
}
