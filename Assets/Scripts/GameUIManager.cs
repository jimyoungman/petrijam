﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;
using System;

public class GameUIManager : MonoBehaviour
{
    public List<FoodItemView> FoodOptions;
    public GameManager GM;
    private List<FoodData> _foods = new List<FoodData>();
    private List<JobData> _jobs = new List<JobData>();

    public HealthMeasureView MuscleHealthView;
    public HealthMeasureView DentalHealthView;
    public HealthMeasureView AcneHealthView;
    public HealthMeasureView HeartHealthView;

    public Text DayText;

    public AudioManager AudioManager;

    public JobView JobView;

    public WinView WinView;

    public bool Busy = false;
    public bool Selected = false;


    private void Start()
    {
        AudioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        foreach (FoodItemView foodItem in FoodOptions)
        {
            foodItem.SelectFoodItem += SelectFoodItem;
            foodItem.SelectJobItem += SelectJobItem;

        }
    }

    public void SetJobData(JobData jobData)
    {
        JobView.SetJobTargets(jobData);
    }

    public void SetHealthData(PlayerData playerData)
    {
        MuscleHealthView.SetDefault(playerData.MuscleLevel);
        DentalHealthView.SetDefault(playerData.DentalLevel);
        AcneHealthView.SetDefault(playerData.AcneLevel);
        HeartHealthView.SetDefault(playerData.HeartLevel);
    }

    public void UpdateHealthViews(PlayerData playerData)
    {
        MuscleHealthView.UpdateValue(playerData.MuscleLevel);
        DentalHealthView.UpdateValue(playerData.DentalLevel);
        AcneHealthView.UpdateValue(playerData.AcneLevel);
        HeartHealthView.UpdateValue(playerData.HeartLevel);
    }

    public void HideWin()
    {
        WinView.HideWin();
    }

    public void SelectFoodItem(FoodData foodData)
    {
        if (!Selected)
        {
            AudioManager.Play("Tap");
            Selected = true;
            StartCoroutine(DropOptions());
            GM.OnSelectMeal(foodData);
        }
    }

    public void SelectJobItem(JobData jobData)
    {
        if (!Selected)
        {
            AudioManager.Play("Tap");
            Selected = true;
            StartCoroutine(DropOptions());
            GM.OnSelectJob(jobData);
        }
    }

    public void SetupCurrentState(GameState gameState, Foods foods, Jobs jobs)
    {
        StopCoroutine(RefreshOptions(SetFoods));
        StopCoroutine(RefreshOptions(SetJobs));
        switch (gameState)
        {
            case GameState.Setup:
                break;
            case GameState.JobSelect:
                _jobs = jobs.JobData.ToList();
                StartCoroutine(RefreshOptions(SetJobs));
                break;
            case GameState.SelectBreakfast:
                _foods = foods.FoodData.Where(i => i.FoodType == FoodTypes.Breakfast).ToList();
                StartCoroutine(RefreshOptions(SetFoods));
                break;
            case GameState.SelectSnack:
                _foods = foods.FoodData.Where(i => i.FoodType == FoodTypes.Snack).ToList();
                StartCoroutine(RefreshOptions(SetFoods));

                break;
            case GameState.SelectLunch:
                _foods = foods.FoodData.Where(i => i.FoodType == FoodTypes.Lunch).ToList();
                StartCoroutine(RefreshOptions(SetFoods));

                break;
            case GameState.SelectActivity:
                _foods = foods.FoodData.Where(i => i.FoodType == FoodTypes.Exercise).ToList();
                StartCoroutine(RefreshOptions(SetFoods));

                break;
            case GameState.SelectDinner:
                _foods = foods.FoodData.Where(i => i.FoodType == FoodTypes.Dinner).ToList();
                StartCoroutine(RefreshOptions(SetFoods));

                break;
            case GameState.JobDecision:
                break;
            default:
                break;
        }
    }

    public void ShowWin(bool won)
    {
        WinView.ShowWin(won);
    }

    private IEnumerator DropOptions()
    {
        while (Busy)
        {
            yield return null;
        }
        Busy = true;
        foreach (FoodItemView foodItem in FoodOptions)
        {
            foodItem.transform.DOLocalMoveY(-750, .6f).SetEase(Ease.InBack);
            AudioManager.Play("Woosh");
            yield return new WaitForSeconds(0.35f);
        }
        yield return new WaitForSeconds(0.25f);
        Busy = false;
    }


    private IEnumerator RefreshOptions(Action OnAnimMid)
    {
        
        while(Busy)
        {
            yield return null;
        }
        OnAnimMid?.Invoke();
        Selected = false;
        foreach (FoodItemView foodItem in FoodOptions)
        {
            if (foodItem.transform.localPosition.y < -152)
            {
                foodItem.transform.DOLocalMoveY(-150, .6f).SetEase(Ease.OutBack);
                AudioManager.Play("Woosh");
                yield return new WaitForSeconds(0.35f);

            }
        }

    }

    private void SetFoods()
    {
        List<FoodData> foods = _foods;
        
        foreach (FoodItemView foodItem in FoodOptions)
        {
            if(foods.Count == 0)
            {
                foods = _foods;
            }
            int optionSelected = UnityEngine.Random.Range(0, foods.Count);
            foodItem.SetFood(foods[optionSelected]);
            foods.Remove(foods[optionSelected]);
        }
    }

    private void SetJobs()
    {
        List<JobData> jobs = _jobs;

        foreach (FoodItemView foodItem in FoodOptions)
        {
            if(jobs.Count == 0)
            {
                jobs = _jobs;
            }
            int optionSelected = UnityEngine.Random.Range(0, _jobs.Count);
            foodItem.SetJob(_jobs[optionSelected]);
            _jobs.Remove(_jobs[optionSelected]);
        }
    }
}
