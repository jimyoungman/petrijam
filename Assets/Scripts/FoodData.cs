﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FoodData : ISerializationCallbackReceiver
{
    public string ID;
    public float Calories;
    public float Carbohydrates;
    public float Fats;
    public float Protien;
    public float Sugar;
    [NonSerialized]public FoodTypes FoodType;
    public string Type;
    public string SpriteID;

    public void OnAfterDeserialize()
    {
        if (!Enum.TryParse(Type, out FoodType))
        {
            if (!string.IsNullOrEmpty(Type))
            {
                Debug.LogError($"Error parsing FoodType {Type} while deserializing FoodData: {ID}");
            }
        }
    }

    public void OnBeforeSerialize()
    {
        
    }
}
