﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public float CalorieAverage;
    [NonSerialized]public List<FoodData> FoodData = new List<FoodData>();
    public float Weight;
    public float Height;
    public float DentalLevel;
    public float AcneLevel;
    public float HeartLevel;
    public float MuscleLevel;
    public float BMI;
    public float EnergyLevel;
    public int Age;

    private float _weightModifier = 10f;
    private float _calsPerKG = 3500f;

    private float _averageProtien = .20f;
    private float _muscleMultiplier = .3f;


    private float _carbCalPercentAverage = 0.35f;
    private float _acneMultiplier = .5f;

    private float _fatCalPercentAverage = 0.17f;
    private float _heartMultiplier = .3f;

    private float _sugarsCalPercentAverage = 0.07f;
    private float _dentalMultiplier = 1f;


    public void AddFood(FoodData foodData)
    {
        FoodData.Add(foodData);
    }

    public void EndDay()
    {
        float Cals = 0;
        float Carbs = 0;
        float Fats = 0;
        float Sugars = 0;
        float Protien = 0;
        float ExerciseCals = 0;

        foreach (FoodData food in FoodData)
        {
            if (food.FoodType == FoodTypes.Exercise)
            {
                ExerciseCals += food.Calories;
            }
            else
            {
                Cals += food.Calories;
                Carbs += food.Carbohydrates;
                Fats += food.Fats;
                Sugars += food.Sugar;
                Protien += food.Protien;
            }
            
        }

        //Muscle
        Fats += CalculateMuscle(Protien, Cals, ExerciseCals);

        //Acne
        CalculateAcne(Carbs, Cals);

        //Heart
        CalculateHeart(Fats, Cals);

        //Dental
        CalculateDental(Sugars, Cals);

        //Weight
        float CalDifference = Cals - ExerciseCals - CalorieAverage;
        Debug.Log($"Average Cals: {CalorieAverage}, Cals Eaten: {Cals - ExerciseCals}");
        AdjustWeight(CalDifference);

        CalculateAverageCalories();

        FoodData.Clear();
    }

    private float CalculateMuscle(float Protien, float Cals, float ExerciseCals)
    {
        float calProtien = Protien * 4;

        float percentProtien = 0;
        if (Cals > 0)
        {
            percentProtien = (calProtien / Cals) * (ExerciseCals / calProtien);
        }
        float protienDiff = percentProtien - _averageProtien;
        float MuscleChange = protienDiff * _muscleMultiplier;
        MuscleLevel += MuscleChange;

        Debug.Log($"Muscle Change: {MuscleChange}");

        if (MuscleLevel > 1)
        {
            MuscleLevel = 1;
        }
        else if (MuscleLevel < 0)
        {
            MuscleLevel = 0;
        }

        //Convert excess to Fats, can be negative
        return (calProtien - ExerciseCals) / 4;

    }

    private void CalculateDental(float Sugars, float Cals)
    {
        float calSugars = Sugars * 4;
        float percentSugars = 0;
        if (Cals > 0)
        {
            percentSugars = calSugars / Cals;
        }

        float sugarsOver = percentSugars - _sugarsCalPercentAverage;

        float DentalChange = sugarsOver * _dentalMultiplier;
        DentalLevel += DentalChange;

        Debug.Log($"Dental Change: {DentalChange}");

        if (DentalLevel > 1)
        {
            DentalLevel = 1;
        }
        else if (DentalLevel < 0)
        {
            DentalLevel = 0;
        }
    }

    public bool CheckWin(JobData chosenJob)
    {
        if (CheckInBounds(MuscleLevel, chosenJob.Muscle)
            && CheckInBounds(AcneLevel, chosenJob.Acne)
            && CheckInBounds(DentalLevel, chosenJob.Dental)
            && CheckInBounds(HeartLevel, chosenJob.Heart))
        {
            return true;
        }
        return false;
    }

    public bool CheckInBounds(float player, float job)
    {
        if(player < job + 0.1
            && player > job -.1f)
        {
            return true;
        }
        return false;
    }

    private void CalculateHeart(float Fats, float Cals)
    {
        float calFat = Fats * 9;
        float percentFats = 0;
        if (Cals > 0)
        {
            percentFats = calFat / Cals;
        }
        float fatsOver = percentFats - _fatCalPercentAverage;

        float HeartChange = fatsOver * _heartMultiplier;
        HeartLevel += HeartChange;

        Debug.Log($"Heart Change: {HeartChange}");

        if (HeartLevel > 1)
        {
            HeartLevel = 1;
        }
        else if (HeartLevel < 0)
        {
            HeartLevel = 0;
        }
    }

    private void CalculateAcne(float Carbs, float Cals)
    {
        float calCarb = Carbs * 4;
        float percentCarb = 0;
        if (Cals > 0)
        {
            percentCarb = calCarb / Cals;
        }
        float carbsOver = percentCarb - _carbCalPercentAverage;

        float AcneChange = carbsOver * _acneMultiplier;
        AcneLevel += AcneChange;

        Debug.Log($"Acne Change: {AcneChange}");

        if (AcneLevel > 1)
        {
            AcneLevel = 1;
        }
        else if(AcneLevel < 0)
        {
            AcneLevel = 0;
        }
    }

    private void AdjustWeight(float CalDifference)
    {
        float weightChange = (CalDifference / _calsPerKG) * _weightModifier;
        Weight += weightChange;

        Debug.Log($"Weight Change: {weightChange}");
    }

    public void Setup()
    {
        Height = UnityEngine.Random.Range(150, 200);
        Weight = UnityEngine.Random.Range(55, 130);
        Age = UnityEngine.Random.Range(18, 60);

        CalculateBMI();
        CalculateAverageCalories();

        DentalLevel = UnityEngine.Random.Range(0.25f, 0.75f);
        AcneLevel = UnityEngine.Random.Range(0.25f, 0.75f);
        HeartLevel = UnityEngine.Random.Range(0.25f, 0.75f);
        EnergyLevel = UnityEngine.Random.Range(0.25f, 0.75f);
        MuscleLevel = UnityEngine.Random.Range(0.25f, 0.75f);

        FoodData.Clear();
    }

    public void CalculateBMI()
    {
        BMI = Weight / ((Height/100) * (Height/100));
    }

    public void CalculateAverageCalories()
    {
        //This is the mens calculation as I have no genders in this game im just using his one
        CalorieAverage = ((10 * Weight) + (6.25f * Height) - (5 * Age) + 5) * 1.25f;
    }
}
