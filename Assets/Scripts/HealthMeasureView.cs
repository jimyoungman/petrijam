﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HealthMeasureView : MonoBehaviour
{
    public Image RedArrow;
    public Image BlueArrow;
    public Text HealthValueText;

    public AudioManager AudioManager;

    public float HealthValue = 0.5f;

    private void Start()
    {
        AudioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    public void SetDefault(float value)
    {
        HealthValue = value;
        HealthValueText.text = HealthValue.ToString("F2");
    }

    public void UpdateValue(float updatedValue)
    {
        
        if(updatedValue > HealthValue)
        {
            StartCoroutine(UpArrow());
        }
        else if(updatedValue < HealthValue)
        {

            StartCoroutine(DownArrow());
        }
        HealthValue = updatedValue;
        HealthValueText.text = HealthValue.ToString("F2");

    }

    private IEnumerator UpArrow()
    {
        Debug.Log($"Running up Arrow");

        RedArrow.DOFade(1, 0);
        RedArrow.transform.localPosition = new Vector3(0, -100, 0);
        RedArrow.transform.localScale = new Vector3(0, 0, 0);
        AudioManager.Play("Woosh");
        RedArrow.transform.DOScale(1, 2);
        RedArrow.transform.DOLocalMoveY(100, 2);
        yield return new WaitForSeconds(2);
        RedArrow.DOFade(0, 1);
        Debug.Log($"finishing up Arrow");

    }

    private IEnumerator DownArrow()
    {
        Debug.Log($"Running down Arrow");

        BlueArrow.DOFade(1, 0);
        BlueArrow.transform.localPosition = new Vector3(0, 100, 0);
        BlueArrow.transform.localScale = new Vector3(0, 0, 0);
        AudioManager.Play("Woosh");
        BlueArrow.transform.DOScale(1, 2);
        BlueArrow.transform.DOLocalMoveY(-100, 2);
        yield return new WaitForSeconds(2);
        BlueArrow.DOFade(0, 1);
        Debug.Log($"finishing down Arrow");

    }
}
